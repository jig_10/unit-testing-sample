import simpleData from '@/simpleData.vue';
import {mount , shallowMount} from '@vue/test-utils';
import { tsExternalModuleReference } from '@babel/types';
import axios from 'axios';

jest.mock('axios');

test('Check name value accesing data property', () => {
    const wrapper = mount(simpleData);
    expect(wrapper.vm.name).toBe('Jignesh');
})

test('Check accesing prop property', () => {
    const wrapper = mount(simpleData, {
        propsData: {
            title : "JigneshJain"
        }
    });
   
    console.log(wrapper.props());
    console.log(wrapper.vm.$props.title);

    expect(wrapper.props().title).toBe('JigneshJain');

})


test('Check accesing methods property', () => {
    const wrapper = mount(simpleData, {
        propsData: {
            title : "JigneshJain"
        }
    });

    wrapper.vm.increment_counter();
    expect(wrapper.vm.count).toBe(2);

})

test('Check api get call test for title', async () => {
    const wrapper = mount(simpleData);
    axios.get.mockResolvedValue({
        data : {
            title : "JigneshAPI"
        }
    })
    const title = await wrapper.vm.getMyTitle();
    console.log(title);
    expect(title).toBe('JigneshAPI')

});

test('Check api get call test for name', async () => {
    const wrapper = mount(simpleData);
    axios.get.mockResolvedValue({
        data : {
            name : "JigneshName"
        }
    })
    const name = await wrapper.vm.getMyName();
    console.log(name);
    expect(name).toBe('JigneshName')

});

test('Check api post call test for name', async () => {
    const wrapper = mount(simpleData);
    axios.post.mockResolvedValue({
        data : {
            status : "success"
        }
    })

    const status = await wrapper.vm.postMyName();
    console.log(status);
    expect(status).toBe('success');

});









