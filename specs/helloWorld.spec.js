import HelloWorld from '@/HelloWorld.vue';
import {mount , shallowMount} from '@vue/test-utils';

test('Check text Hello World', () => {
  const wrapper  = mount(HelloWorld);
  expect(wrapper.html()).toMatchSnapshot();
})

