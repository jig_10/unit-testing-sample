import {mount , shallowMount} from '@vue/test-utils';
import TestComponent from '@/test.vue';
import List from '@/list.vue';

test('first-sepc',() => {
    // console.log(TestComponent);

})
test('mount a component',() => {
    const wrapper = mount(TestComponent)
    expect(wrapper.html()).toMatchSnapshot();
})

test('mount a component by chaning prop value',() => {
    const wrapper = mount(TestComponent,{
        propsData: {
            value:'Test New Val'
        }
    })
    expect(wrapper.html()).toMatchSnapshot();
})

test('ListComponents Shallow',() => {
    // console.log(mount(List).html());
    // console.log(shallowMount(List).html());
})

test('ListComponent', async () => {
    const wrapper = mount(List);
    const movies = wrapper.vm.marvelMovies;
    wrapper.setData({marvelMovies:[...movies,'Harry Potter']});
    await wrapper.vm.$nextTick();
    expect(wrapper.html()).toMatchSnapshot();
})



